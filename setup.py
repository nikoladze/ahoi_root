#!/usr/bin/env python

from setuptools import setup

setup(
    name="ahoi_root",
    version=0.2,
    description="ROOT TTree frontend for the ahoi module",
    author="Nikolai Hartmann",
    author_email="nikoladze@posteo.de",
    packages=["ahoi_root"],
    scripts=["scripts/ahoi_root"],
    url="https://gitlab.com/nikoladze/ahoi_root",
    install_requires=[
        "ahoi",
        "numpy",
        "pandas>=0.24.2",
        "uproot",
        "formulate",
        "h5py",
        "tables",
        "pyaml",
    ],
)
