# A Horrible Optimization Instrument (AHOI) - ROOT frontend

This code offers some helpers to do grid scans over all combinations of certain
selection criteria. The data input format should be flat ntuples in the ROOT format.
If you are a new user, i recomended to directly use the [ahoi
module](https://gitlab.com/nikoladze/ahoi) instead of this package.

However, this package also supports using the [ahoi
module](https://gitlab.com/nikoladze/ahoi) as a backend.
To make use of that pass `method="ahoi_uproot"` when initializing
the `Scanner` class.

The code is not very well documented - have a look at the
[examples](examples). Install the package by e.g.

```
git clone git@gitlab.com:nikoladze/ahoi_root.git
pip install --user -e ./ahoi_root
```
