import unittest
import logging
from .is_close import isclose

logger = logging.getLogger("FloatTester")

class FloatTester(unittest.TestCase):

    """Base Test for everything that needs to compare floats, histograms etc"""

    def assertClose(self, a, b, rel_tol=1e-8):
        """Test if two floats are equal"""
        logger.debug("{} ?~= {}".format(a, b))
        try:
            self.assertTrue(isclose(a, b, rel_tol=rel_tol))
        except AssertionError as e:
            raise AssertionError("{} is not close enough to {}".format(a, b))


    def assertEqualHists(self, hist1, hist2):
        """
        Test if two histograms are equal (bin contents equal according to
        FloatTester.assertClose
        """
        self.assertEqual(hist1.GetNbinsX(), hist2.GetNbinsX())
        for i in range(hist1.GetNbinsX()+1):
            content1 = hist1.GetBinContent(i)
            content2 = hist2.GetBinContent(i)
            self.assertClose(content1, content2)
            error1 = hist1.GetBinError(i)
            error2 = hist1.GetBinError(i)
            self.assertClose(error1, error2)
        self.assertClose(hist1.Integral(), hist2.Integral())
