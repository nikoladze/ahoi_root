import random
import unittest
import tempfile
import shutil
import os
import subprocess
import atexit
import numpy as np
from .floatTester import FloatTester
from .helpers import workInTempDir
from ..tool import Scanner, cd, Process, load_from_dir, ConfigChangedException

import ROOT

import logging

logger = logging.getLogger("ahoi_root.test")

# logging.basicConfig()
# logging.getLogger("ahoi_root").setLevel(logging.DEBUG)
# logging.getLogger("FloatTester").setLevel(logging.DEBUG)

methods = ["codegen", "ahoi_uproot"]


class TestIndices(unittest.TestCase):
    def test_obvious(self):
        self.assertEqual(
            Scanner.get_combination_index([1, 6, 9, 4], [10, 10, 10, 10]), 1694
        )

    def test_1237(self):
        self.assertEqual(
            Scanner.get_combination_index([1, 1, 3, 6], [12, 2, 8, 19]), 519
        )

    def test_transform_back_and_forth(self):
        ranges = [random.randint(1, 50) for i in range(5)]
        indices = [random.randint(0, i - 1) for i in ranges]
        index = Scanner.get_combination_index(indices, ranges)
        indices2 = Scanner.get_cut_indices(index, ranges)
        self.assertEqual(tuple(indices), tuple(indices2))

    def test_equal_c_python(self):
        ranges = [random.randint(1, 50) for i in range(5)]
        indices = [random.randint(0, i - 1) for i in ranges]
        names = ["testvar_{}".format(i) for i in range(5)]
        expr = Scanner.get_combination_index_expr(names, ranges)
        for name, index in zip(names, indices):
            ROOT.gInterpreter.Declare("int i_{} = {};".format(name, index))
        ROOT.gInterpreter.Declare("int testindex = {};".format(expr))
        self.assertEqual(ROOT.testindex, Scanner.get_combination_index(indices, ranges))


class TestYieldsOnTheFlyNtuple(FloatTester):
    def test_simple_ntuple(self):
        for method in methods:
            with workInTempDir(baseDir="/tmp"):
                rf = ROOT.TFile.Open("testntuple.root", "RECREATE")
                t = ROOT.TNtuple("ntuple", "", "x:y:z")
                for i in range(10):
                    t.Fill(1, 2, 3)
                t.Fill(2, 2, 2)
                t.Fill(10, 10, 10)
                rf.Write()
                rf.Close()
                s = Scanner(
                    "test", n_subsets=1, weight="0.5", do_compile=False, method=method
                )
                s.add_process("test", "testntuple.root", "ntuple", is_signal=False)
                s.add_cut("x", [0, 1, 2, 3], comparator=">")
                s.add_cut("y", [0, 1, 2, 3], comparator=">")
                s.add_cut("z", [0, 1, 2, 3], comparator=">")

                self.assertEqual(
                    s.get_yields("test", number_type="n")[
                        Scanner.get_combination_index([2, 2, 2], s.ranges)
                    ],
                    1,
                )  # one should pass all > 3
                self.assertEqual(
                    s.get_yields("test", number_type="n")[
                        Scanner.get_combination_index([0, 0, 0], s.ranges)
                    ],
                    12,
                )  # all should pass all > 0
                self.assertEqual(
                    s.get_yields("test", number_type="n")[
                        Scanner.get_combination_index([1, 1, 1], s.ranges)
                    ],
                    2,
                )  # 2 should pass all > 1
                # since we weight to 0.5 all sumw should be half of n
                # and sumw2 0.25 of n
                n = s.get_yields("test", number_type="n")
                sumw = s.get_yields("test", number_type="sumw")
                sumw2 = s.get_yields("test", number_type="sumw2")
                for i in range(s.ncombinations):
                    self.assertClose(0.5 * n[i], sumw[i])
                    self.assertClose(0.25 * n[i], sumw2[i])


class TestYieldsTMVAExample(FloatTester):

    """
    Test if the yields for certain cut combinations end up to be correct.
    """

    @classmethod
    def setUpClass(cls):
        cls.method = "codegen"
        if not os.path.exists("/tmp/tmva_class_example.root"):
            with cd("/tmp"):
                subprocess.call(
                    ["wget", "http://root.cern.ch/files/tmva_class_example.root"]
                )

    def setUp(self):
        self.temp_dir = tempfile.mkdtemp(dir="/tmp", prefix="ahoi_test_")
        self.cwd = os.path.abspath(os.getcwd())
        os.chdir(self.temp_dir)
        self.scanner = Scanner(
            "ahoi_test", n_subsets=1, do_compile=False, method=self.method
        )
        self.scanner.add_process(
            "test", "/tmp/tmva_class_example.root", "TreeB", is_signal=False
        )
        self.scanner.add_cut("var1", [0, 1, 2])
        self.scanner.add_cut("var2", [0, 1, 2])
        self.scanner.add_cut("var3", [0, 1, 2])

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.temp_dir)

    def compare_scanner_draw(self, rel_tol=1e-8):
        self.array_dict = self.scanner.scan("test")
        for i_bin, bincut in enumerate(self.scanner.bin_cuts):
            total_n = np.zeros(self.scanner.ncombinations)
            total_sumw = np.zeros(self.scanner.ncombinations)
            total_sumw2 = np.zeros(self.scanner.ncombinations)
            for i_subset in range(self.scanner.n_subsets):
                # we can't check TTree::Draw against random subsets,
                # but we can check that the sum of all subsets is
                # equal to what TTree::Draw gives
                total_n += np.array(
                    self.array_dict["bincut{}_subset{}_n".format(i_bin, i_subset)]
                )
                total_sumw += np.array(
                    self.array_dict["bincut{}_subset{}_sumw".format(i_bin, i_subset)]
                )
                total_sumw2 += np.array(
                    self.array_dict["bincut{}_subset{}_sumw2".format(i_bin, i_subset)]
                )
                logger.debug(total_n)
            for combination_number in range(self.scanner.ncombinations):
                n, sumw, sumw2 = self.scanner.get_yields_draw(
                    "test",
                    self.scanner.get_cut_expr(combination_number),
                    bin_index=i_bin,
                )
                self.assertClose(total_n[combination_number], n, rel_tol=rel_tol)
                self.assertClose(total_sumw[combination_number], sumw, rel_tol=rel_tol)
                self.assertClose(
                    total_sumw2[combination_number], sumw2, rel_tol=rel_tol
                )

    def test_yields_no_weight(self):
        self.scanner.weight = "1"
        self.compare_scanner_draw()

    def test_yields_no_weight_bin_cut(self):
        self.scanner.weight = "1"
        self.scanner.bin_cuts = ["var4>0"]
        self.compare_scanner_draw()

    def test_yields_weight(self):
        self.scanner.weight = "var4"
        self.compare_scanner_draw()

    def test_yields_bin_cuts(self):
        self.scanner.weight = "0.1*var4"
        self.scanner.bin_cuts = ["var4>0", "var4>1", "var4>2"]
        self.compare_scanner_draw(rel_tol=1e-5)

    def test_yields_process_cut(self):
        process = self.scanner.process_dict["test"][0]
        self.scanner.process_dict["test"][0] = Process(
            **dict(process._asdict(), cut="var4>0.5")
        )
        self.compare_scanner_draw()

    def test_yields_subsets(self):
        self.scanner.n_subsets = 10
        self.scanner.subset_fractions = list(range(1, 11))
        self.compare_scanner_draw()

    def test_yields_baseline(self):
        self.scanner.weight = "0.1*var4"
        self.scanner.baseline_selections = ["var4>0", "var4>0.7"]
        self.array_dict = self.scanner.scan("test")
        for i_baseline, baseline_selection in enumerate(
            self.scanner.baseline_selections
        ):
            n, sumw, sumw2 = self.scanner.get_yields_draw("test", baseline_selection)
            self.assertClose(
                self.array_dict["baseline{}_subset0_n".format(i_baseline)], n
            )
            self.assertClose(
                self.array_dict["baseline{}_subset0_sumw".format(i_baseline)],
                sumw,
                rel_tol=1e-5,
            )
            self.assertClose(
                self.array_dict["baseline{}_subset0_sumw2".format(i_baseline)],
                sumw2,
                rel_tol=1e-5,
            )

    def test_compile_vs_no_compile(self):
        self.scanner.do_compile = False
        self.scanner.weight = "0.1*var4"
        self.scanner_c = self.scanner.generate_and_loop("test")
        arrays_no_compile = {}
        for number_type in ["n", "sumw", "sumw2"]:
            arrays_no_compile[number_type] = np.array(
                getattr(self.scanner_c, "bincut0_subset0_{}".format(number_type))
            )
        self.scanner.do_compile = True
        self.scanner.reset_class_uuid()
        shutil.rmtree(self.scanner.workdir)
        self.scanner_c = self.scanner.generate_and_loop("test")
        for number_type in ["n", "sumw", "sumw2"]:
            array_compile = np.array(
                getattr(self.scanner_c, "bincut0_subset0_{}".format(number_type))
            )
            for no_compile, do_compile in zip(
                array_compile, arrays_no_compile[number_type]
            ):
                self.assertClose(no_compile, do_compile)

    def test_restrict_subset(self):
        self.scanner.do_compile = False
        self.scanner.weight = "0.1*var4"
        self.scanner.n_subsets = 10
        self.scanner.subset_fractions = [1.0 for i in range(10)]
        self.scanner_c = None
        arrays_generated_separately = []
        for i_subset in range(self.scanner.n_subsets):
            logger.debug("scan for subset {}".format(i_subset))
            self.scanner.generate_and_dump("test", i_subset=i_subset)
            self.scanner.reset_class_uuid()
            arrays_subset = {}
            for number_type in ["n", "sumw", "sumw2"]:
                arrays_subset[number_type] = self.scanner.get_yields(
                    "test", i_subset=i_subset, number_type=number_type
                )
            arrays_generated_separately.append(arrays_subset)
        shutil.rmtree(self.scanner.workdir)
        for i_subset, arrays in enumerate(arrays_generated_separately):
            logger.debug("Comparing subset {}".format(i_subset))
            for number_type, array_separate in arrays.items():
                array_combined = self.scanner.get_yields(
                    "test", i_subset=i_subset, number_type=number_type
                )
                for number_separate, number_combined in zip(
                    array_separate, array_combined
                ):
                    self.assertClose(number_separate, number_combined)


class TestYieldsTMVAExample_ahoi_uproot(TestYieldsTMVAExample):
    @classmethod
    def setUpClass(cls):
        super(TestYieldsTMVAExample, cls).setUpClass()
        cls.method = "ahoi_uproot"

    def test_compile_vs_no_compile(self):
        # this doesn't make sense here
        pass

    def test_chunkwise(self):
        self.scanner.process_dict["test"][0] = Process(
            **dict(self.scanner.process_dict["test"][0]._asdict(), print_status=1000)
        )
        self.compare_scanner_draw()


class TestROC(FloatTester):
    @classmethod
    def setUpClass(cls):
        if not os.path.exists("/tmp/tmva_class_example.root"):
            with cd("/tmp"):
                subprocess.call(
                    ["wget", "http://root.cern.ch/files/tmva_class_example.root"]
                )

    def setUp(self):
        self.temp_dir = tempfile.mkdtemp(dir="/tmp", prefix="ahoi_test_")
        self.cwd = os.path.abspath(os.getcwd())
        os.chdir(self.temp_dir)
        self.scanner = Scanner("ahoi_test", n_subsets=1, do_compile=False)
        self.scanner.add_process(
            "bkg", "/tmp/tmva_class_example.root", "TreeB", is_signal=False
        )
        self.scanner.add_process(
            "sig", "/tmp/tmva_class_example.root", "TreeS", is_signal=True
        )
        value_range = [-3 + 2 * i for i in range(4)]
        # value_range = [-3+i for i in range(7)]
        self.scanner.add_cut("var1", value_range)
        self.scanner.add_cut("var1", value_range, name="var1_upper", comparator="<")
        self.scanner.add_cut("var2", value_range)
        self.scanner.add_cut("var2", value_range, name="var2_upper", comparator="<")
        self.scanner.add_cut("var3", value_range)
        self.scanner.add_cut("var3", value_range, name="var3_upper", comparator="<")
        self.scanner.add_cut("var4", value_range)
        self.scanner.add_cut("var4", value_range, name="var4_upper", comparator="<")

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.temp_dir)

    def test_roc_single_bin(self):
        self.scanner.do_compile = False
        binning = (0.01, 1, 0.01)
        ar_effs, ar_effb, indices = self.scanner.get_ROC_curve(binning=binning)
        effs_bins = np.arange(*binning)
        for i, (effs_low, effs_high) in enumerate(zip(effs_bins[:-1], effs_bins[1:])):
            logger.debug(
                "checking signal efficiency bin {} - {}".format(effs_low, effs_high)
            )
            roc_bin = self.scanner.get_min_bkg_combinations(
                effs_low, effs_high, cache=True
            )
            if roc_bin is not None:
                effs, effb, index = roc_bin
                logger.debug("Found effs, effb, index = {}, {}, {}".format(*roc_bin))
                self.assertClose(effs[0], ar_effs[i])
                self.assertClose(effb[0], ar_effb[i])
                self.assertEqual(index[0], indices[i])
            else:
                continue


class TestLoad(unittest.TestCase):
    def test_reload_from_dir(self):
        with workInTempDir(baseDir="/tmp"):
            scanner = Scanner("ahoi_test", n_subsets=1, do_compile=False)
            scanner.add_process(
                "test", "/tmp/tmva_class_example.root", "TreeB", is_signal=False
            )
            scanner.add_cut("var1", [0, 1, 2])
            scanner.add_cut("var2", [0, 1, 2])
            scanner.add_cut("var3", [0, 1, 2])
            scanner.generate_and_loop("test")
            scanner = load_from_dir("ahoi_test")
            scanner.reset_class_uuid()
            scanner.generate_and_loop("test")

    def test_detect_change(self):
        with workInTempDir(baseDir="/tmp"):
            scanner = Scanner("ahoi_test", n_subsets=1, do_compile=False)
            scanner.add_process(
                "test", "/tmp/tmva_class_example.root", "TreeB", is_signal=False
            )
            scanner.add_cut("var1", [0, 1, 2])
            scanner.add_cut("var2", [0, 1, 2])
            scanner.add_cut("var3", [0, 1, 2])
            scanner.generate_and_loop("test")
            scanner.reset_class_uuid()

            scanner.add_cut("var4", [1, 2])
            with self.assertRaises(ConfigChangedException):
                scanner.generate_and_loop("test")
