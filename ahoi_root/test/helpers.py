import tempfile, atexit, os, shutil

class workInTempDir:
    """Context manager for changing to a temporary directory that will be
    deleted afterwards. The given dir will be the base directory in
    which the tempdir is created. If not given, the system tmp
    directory will be used. If *skipCleanup* is given the directory is
    not deleted afterwards. Use *prefix* to control the naming format.
    If *cleanAtExit* is set the tmp directory is cleaned at exit of the script,
    not when exiting the context.
    """
    def __init__(self, baseDir=None, skipCleanup=False, prefix="tmp_", cleanAtExit=False):
        self.baseDir = baseDir
        self.tempDir = tempfile.mkdtemp(dir=baseDir, prefix=prefix)
        self.skipCleanup = skipCleanup
        self.cleanAtExit = cleanAtExit

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.tempDir)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)
        if self.skipCleanup:
            return
        if not self.cleanAtExit:
            self.cleanup()
        else:
            atexit.register(self.cleanup)

    def cleanup(self):
        shutil.rmtree(self.tempDir)
