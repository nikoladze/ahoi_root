"""This is an attempt to keep the interface of the old ahoi tool (that uses
ROOT files exclusively) and use the functionality of the new ahoi module.
"""

import os
import re
import logging
import uuid
import yaml
import difflib
import formulate
from collections import namedtuple, OrderedDict
import numpy as np
import h5py
import ROOT
import uproot
import pandas as pd
from tqdm import tqdm
import ahoi

_logger = logging.getLogger("ahoi_root")
_logger.addHandler(logging.NullHandler())

Process = namedtuple(
    "Process", ["name", "filename", "treename", "is_signal", "cut", "print_status"]
)

Cut = namedtuple("Cut", ["name", "varexp", "values", "comparator", "type_name"])


class ConfigChangedException(Exception):
    pass


class cd:
    """
    Context manager for temporarily changing the current working
    directory and creating it if it doesn't exist yet
    """

    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        if not os.path.exists(self.newPath):
            os.mkdir(self.newPath)
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


class Scanner(object):

    """
    Set up a scan over all combinations of cuts for the given variables.

    :param workdir: store the output and temporary files in this directory
    :param bin_cuts: if given, calculate the yields for each given bin
                     cut added on top of each cut combination (can be used to optimise
                     for binned shape fits)
    :param n_subsets: perform scan independently for this number of
                      random subset - can be used for train/test procedures or for
                      averaging over random subsets (default 2)
    :param weight: apply this weight/selection to all events
    :param subset_fractions: if given, use these fractions for all subsets
    :param baseline_selections: store the total yields for these baseline selections (+weight) for all subsets.
    :param do_compile: If true (default), compile the macro with "+" and optimisation flags -
                       you might want to turn this off if you scan many small trees, but
                       for larger trees and large number of cut combinations it's usually
                       worth the overhead.
    :param random_seed: use this random seed for splitting the data
    :param method: method for scanning over cut combinations. One of {"codegen", "ahoi_uproot"}
    :param method_kw: keyword arguments for the method - currently only
                      supported for "ahoi_uproot". They will be passed to `ahoi.scan`
    """

    yield_type = "double"

    # to be used to reproduce the instance (together with cuts and processes)
    _attributes = [
        "bin_cuts",
        "n_subsets",
        "weight",
        "subset_fractions",
        "baseline_selections",
        "do_compile",
        "random_seed",
        "method",
        "method_kw",
    ]

    def __init__(
        self,
        workdir,
        bin_cuts=None,
        n_subsets=2,
        weight="1",
        subset_fractions=None,
        baseline_selections=None,
        do_compile=True,
        random_seed=42,
        method="codegen",
        method_kw=None,
    ):
        self.workdir = workdir
        self.bin_cuts = bin_cuts
        self.n_subsets = n_subsets
        self.weight = weight
        self._subset_fractions = subset_fractions
        self.baseline_selections = baseline_selections
        self.do_compile = do_compile
        self.random_seed = random_seed
        self.method = method
        self.method_kw = method_kw

        self.process_dict = OrderedDict()
        self.cuts = []
        self.yields_cache = {}

        if self.bin_cuts is None:
            # per default use 1 bin without preselection
            self.bin_cuts = ["1"]

        if self._subset_fractions is None:
            self._subset_fractions = [1.0 for i in range(self.n_subsets)]

        if self.baseline_selections is None:
            # per default fill the total yield without preselection
            self.baseline_selections = ["1"]

        self.check_valid_cutexpr(self.weight)
        for cut in self.bin_cuts + self.baseline_selections:
            self.check_valid_cutexpr(cut)

        self.class_uuid = None
        self.reset_class_uuid()

    @property
    def options(self):
        """
        Returns a dictionary that defines the configuration of this instance
        """
        options = {}
        for option in self._attributes:
            options[option] = getattr(self, option)
        options["processes"] = []

        for process_name, process_list in self.process_dict.items():
            for process in process_list:
                options["processes"].append(
                    [list(t) for t in process._asdict().items()]
                )
        options["cuts"] = []
        for cut in self.cuts:
            options["cuts"].append([list(c) for c in cut._asdict().items()])
        return options

    def __repr__(self):
        return "Scanner instance with options:\n\n" + yaml.dump(self.options)

    def _dump_options(self):
        """
        Dump the configuration of this instance to a yaml file
        """

        with cd(self.workdir):
            with open("options.yaml", "w") as of:
                yaml.dump(self.options, of)

    def check_options(self):
        optionspath = os.path.join(self.workdir, "options.yaml")
        if os.path.exists(optionspath):
            with open(optionspath) as f:
                options = yaml.load(f, Loader=yaml.FullLoader)
            if not (options == self.options):
                with open(optionspath) as f:
                    old_config = f.readlines()
                current_config = [
                    line + "\n" for line in yaml.dump(self.options).split("\n")
                ]
                _logger.error("Config differs:")
                for diffline in difflib.unified_diff(
                    old_config, current_config, fromfile="old", tofile="current"
                ):
                    _logger.error(diffline.strip())
                raise ConfigChangedException(
                    "A configuration for this workdir already exists, "
                    "but the current config differs - if you want to "
                    "run a new setup change the workdir or delete it!"
                )
        self._dump_options()

    @property
    def subset_fractions(self):
        """
        Ensure that the number of subset fractions if of equal size of the
        number of subsets and that the values are floats and non-zero
        """
        if not len(self._subset_fractions) == self.n_subsets:
            raise IndexError(
                "The list of subset fractions must have the same length as the number of subsets"
            )
        if any([fraction == 0 for fraction in self._subset_fractions]):
            raise ValueError("All subset fractions have to be non-zero!")
        return [float(i) for i in self._subset_fractions]

    @subset_fractions.setter
    def subset_fractions(self, value):
        self._subset_fractions = value

    def reset_class_uuid(self):
        "Generate a unique suffix for the names of generated classes"
        self.class_uuid = uuid.uuid4().hex

    def get_class_name(self, process_name):
        return "scanner_{}_{}".format(process_name, self.class_uuid)

    def add_process(
        self, name, filename, treename, is_signal, cut=None, print_status=100000
    ):

        """
        Add a new process (to either signal or background), using a tree
        in a file. Processes are grouped by name.

        :param cut: apply this preselection string for this specific process (will also be multiplied with weights)
        :param print_status: log current event number every this number of events
        """

        if cut is not None:
            self.check_valid_cutexpr(cut)
        else:
            cut = "1"

        if not os.path.isabs(filename):
            new_filename = os.path.abspath(filename)
            _logger.warning(
                "Converting path {} to absolute path: {}".format(filename, new_filename)
            )
            filename = new_filename

        if not name in self.process_dict:
            self.process_dict[name] = []
        process = Process(name, filename, treename, is_signal, cut, print_status)
        consistent_fields = ["is_signal", "cut", "print_status"]
        for field in consistent_fields:
            if any(
                [
                    getattr(other_process, field) != getattr(process, field)
                    for other_process in self.process_dict[name]
                ]
            ):
                raise ValueError(
                    'Processes with the same name have to share a common "{}" attribute'.format(
                        field
                    )
                )
        self.process_dict[name].append(process)

    def add_cut(self, varexp, values, comparator=">", name=None, type_name="double"):

        """
        Add a cut variable, given by an expr and a list of values.

        :param varexp: the expression that defines the variables
        :param values: list of values to scan
        :param comparator: the operator to compare the values (default: ">")
        :param name: name for the variable (will be replaced by varexp by default)
        :param type_name: (c-)type name of the cut value for
                          comparison.  The default value of "double"
                          should be fine for basically all comparisons.
                          (even "=="/">=" etc with int, although it
                          doesn't hurt to use "int" in this case).
        """

        self.check_valid_cutexpr(varexp)

        if name is None:
            name = varexp

        self.cuts.append(Cut(name, varexp, values, comparator, type_name))

    @staticmethod
    def check_valid_cutexpr(cutexpr):
        """Check if the cutexpr is valid (runs in generated code).

        Raises a ValueError if not."""

        if ("**" in cutexpr) or ("^" in cutexpr):
            raise ValueError(
                "** or ^ can't be used in a cut expression - use pow() for exponentiation"
            )

    @staticmethod
    def is_in_cutexpr(branchname, cutexpr):
        if cutexpr is None:
            return False
        # hopefully i didn't forget anything
        branchnames = re.split(
            "\!|\[|\]|>|<|\||\&|\=|\?|\:|\+|\-|\(|\)|\*|\/|\s|\,", cutexpr
        )
        if branchname in branchnames:
            return True
        else:
            if branchname in cutexpr:
                _logger.warning(
                    "The expression {} contains the branch "
                    "name {}, but was not determined to be actually used - "
                    "please check if this is fine!".format(cutexpr, branchname)
                )
            return False

    def branch_used(self, branchname):
        """
        Determine wether a branch is used for any cut or
        varexp. Need to be careful not to forget anything ;)
        """
        if self.is_in_cutexpr(branchname, self.weight):
            return True
        for cut in self.cuts:
            if self.is_in_cutexpr(branchname, cut.varexp):
                return True
        for process_list in self.process_dict.values():
            for process in process_list:
                if self.is_in_cutexpr(branchname, process.cut):
                    return True
        for cut in self.bin_cuts + self.baseline_selections:
            if self.is_in_cutexpr(branchname, cut):
                return True
        return False

    @staticmethod
    def get_combination_index_expr(indexnames, indexranges):
        """
        return c expression to calculate
        one index out of n indices

        should calculate the same as the python function
        get_combination_index of this class
        """

        # make compatible with numpy indexing
        indexnames = indexnames[::-1]
        indexranges = indexranges[::-1]

        # special case for only 1 variable
        if len(indexnames) == 1:
            return "i_{0}".format(indexnames[0])
        # else
        funexpr = "i_{0}+({1}*(".format(indexnames[0], indexranges[0])
        for name, range in zip(indexnames[1:-1], indexranges[1:-1]):
            funexpr += "i_{0}+({1}*(".format(name, range)
        funexpr += "i_{0}".format(indexnames[-1])
        funexpr += "{}".format(")" * (len(indexnames) - 1) * 2)
        return funexpr

    @staticmethod
    def get_combination_index(indices, ranges):
        """
        Get the 1-dimensional index for a list of indices for the
        individual cuts.

        In the past i used to do::

            i1d = indices[0]
            factor = ranges[0]
            for i, max in zip(indices[1:], ranges[1:]):
                i1d += i*factor
                factor *= max
            return i1d

        Now using numpy, since it also performs some checks. Note the indices
        and ranges are reversed for the numpy calculation.
        """
        return np.ravel_multi_index(indices, ranges)

    @staticmethod
    def get_cut_indices(index, ranges):
        """
        Return the individual indices for all cuts from a single cut combination index.

        In the past i used to do::

            indices = []
            factor = 1
            mod = i
            for j in ranges:
                factor *= j
            for j in reversed(ranges):
                factor = factor/j
                div = int(mod)//int(factor)
                mod = int(mod)%int(factor)
                indices.append(div)
            return [i for i in reversed(indices)]

        Now using numpy, since it also performs some checks. Note the indices
        and ranges are reversed for the numpy calculation.
        """
        return np.unravel_index(index, tuple(ranges))

    @property
    def ranges(self):
        return [len(c.values) for c in self.cuts]

    @property
    def ncombinations(self):
        ncombinations = 1
        for cut in self.cuts:
            ncombinations *= len(cut.values)
        return ncombinations

    def get_tchain(self, process_name):
        chain = ROOT.TChain()
        for process in self.process_dict[process_name]:
            chain.AddFile(process.filename, -1, process.treename)
        return chain

    def get_cut_expr(self, index):
        "Returns the total cut expr (TTree::Draw style) for a given cut combination index"
        cut_exprs = []
        indices = self.get_cut_indices(index, self.ranges)
        for cut, cut_index in zip(self.cuts, indices):
            cut_exprs.append(
                "{}{}{}".format(cut.varexp, cut.comparator, cut.values[cut_index])
            )
        return "&&".join(cut_exprs)

    def get_cut_values(self, index):
        "Returns a dictionary of cut names and the cut values for a given cut combination index"
        indices = self.get_cut_indices(index, self.ranges)
        cut_values = OrderedDict()
        for cut, cut_index in zip(self.cuts, indices):
            cut_values[cut.name] = cut.values[cut_index]
        return cut_values

    def get_yields_draw(self, process_name, cut_expr, bin_index=0, custom_weight=None):

        """
        Returns n, sumw, sumw2 for the given cut expression by using
        TChain.Draw. If custom_weight is given, it will be used
        instead of the global weight and selection for the process and index.

        TODO: maybe interface this with MPF and memoization stuff at some time
        """

        h_yield = ROOT.TH1D("h_yield", "", 1, 0.5, 1.5)
        tchain = self.get_tchain(process_name)
        ref_process = self.process_dict[process_name][0]
        if custom_weight is None:
            total_cut = "({})*({})*({})*({})".format(
                self.weight, ref_process.cut, cut_expr, self.bin_cuts[bin_index]
            )
        else:
            total_cut = "({})*({})".format(cut_expr, custom_weight)
        _logger.info("Fetching yields for expression " + total_cut)
        tchain.Draw("1>>h_yield", total_cut, "goff")
        err = ROOT.Double()
        sumw = h_yield.IntegralAndError(1, 1, err)
        sumw2 = float(err) ** 2
        n = h_yield.GetEntries()
        del h_yield
        return n, sumw, sumw2

    def get_sum_yields_draw(self, *process_names, **kwargs):

        cut_expr = kwargs.pop("cut_expr", "1")
        bin_index = kwargs.pop("bin_index", 0)
        custom_weight = kwargs.pop("custom_weight", None)
        if kwargs:
            raise KeyError("Got unexpected kwargs: {}".format(kwargs))

        total_n = 0.0
        total_sumw = 0.0
        total_sumw2 = 0.0
        for process_name in process_names:
            n, sumw, sumw2 = self.get_yields_draw(
                process_name, cut_expr, bin_index=bin_index, custom_weight=custom_weight
            )
            total_n += n
            total_sumw += sumw
            total_sumw2 += sumw2
        return total_n, total_sumw, total_sumw2

    def generate(self, process_name, i_subset=None, i_bin=None):

        """
        generate c code which is using loop over all events and scan all
        cut combinations for each event.

        TODO: Should be rewritten to directly load stuff instead of dumping .C/.h

        :param process_name: generate for code for the process with this name
        :param i_subset: if not None, restrict scan to the given subset index
        :param i_bin: if not None, restrict scan to the given bin index
        """

        self.check_options()

        restrict_subset = i_subset
        restrict_bin = i_bin

        if restrict_bin is not None:
            self.check_arguments(i_bin=restrict_bin)
        if restrict_subset is not None:
            self.check_arguments(i_subset=restrict_subset)

        bin_index_cuts = []
        for i_bin_cut, bin_cut in enumerate(self.bin_cuts):
            if (restrict_bin is not None) and (not i_bin_cut == restrict_bin):
                continue
            bin_index_cuts.append((i_bin_cut, bin_cut))

        subset_indices = []
        for i_subset in range(self.n_subsets):
            if (restrict_subset is not None) and (not i_subset == restrict_subset):
                continue
            subset_indices.append(i_subset)

        # reference process for shared attributes
        ref_process = self.process_dict[process_name][0]

        total_weight = "({})*({})".format(self.weight, ref_process.cut)

        with cd(self.workdir):

            chain = self.get_tchain(process_name)

            class_name_dot_C = self.get_class_name(process_name) + ".C"
            class_name_dot_h = self.get_class_name(process_name) + ".h"

            if os.path.exists(class_name_dot_C):
                raise IOError(
                    class_name_dot_C
                    + " already exists - delete the working directory or use Scanner.reset_class_uuid"
                )
            if os.path.exists(class_name_dot_h):
                raise IOError(
                    class_name_dot_h
                    + " already exists - delete the working directory or use Scanner.reset_class_uuid"
                )

            chain.MakeClass(self.get_class_name(process_name))

            # first, generate and edit the header

            with open(class_name_dot_h) as f:
                headerlines = f.readlines()

            with open(class_name_dot_h, "w") as of:
                in_constructor = False
                of.write('#include "TRandom.h"')
                for line in headerlines:

                    # ugly hack - need to remove include protection
                    # guards, since we concatenate .h and .C
                    if any(
                        [(s in line) for s in ["ifdef", "ifndef", "define", "endif"]]
                    ):
                        # we still need that one, lets hope it stays the only one in future ROOT versions
                        if not "SINGLE_TREE" in line:
                            continue

                    # declare members
                    if "public" in line:
                        of.write(line)
                        for cut in self.cuts:

                            # indices to track all cutvalues and wether they passed in the current event
                            of.write("int i_{0};\n".format(cut.name))
                            of.write(
                                "std::vector<bool> {}_pass;\n".format(
                                    cut.name, len(cut.values)
                                )
                            )
                            of.write(
                                "std::vector<{}> {}_cuts;\n".format(
                                    cut.type_name, cut.name
                                )
                            )
                        of.write("std::vector<bool> pass_bin_cuts;\n")

                        # vectors that store the yields for all subsets and bin cuts
                        for i_bin_cut, bin_cut in bin_index_cuts:
                            for i_subset in subset_indices:
                                for number_type in ["n", "sumw", "sumw2"]:
                                    of.write(
                                        "std::vector<{}> bincut{}_subset{}_{};\n".format(
                                            self.yield_type,
                                            i_bin_cut,
                                            i_subset,
                                            number_type,
                                        )
                                    )
                        for i_subset in subset_indices:
                            for i_baseline, baseline_selection in enumerate(
                                self.baseline_selections
                            ):
                                for number_type in ["n", "sumw", "sumw2"]:
                                    of.write(
                                        "{} baseline{}_subset{}_{};\n".format(
                                            self.yield_type,
                                            i_baseline,
                                            i_subset,
                                            number_type,
                                        )
                                    )
                    elif "{0}::{0}".format(self.get_class_name(process_name)) in line:
                        in_constructor = True
                        of.write(line)
                    else:
                        of.write(line)

                        # initialise members
                        if in_constructor:
                            in_constructor = False
                            for cut in self.cuts:

                                # indices to track all cutvalues and wether they passed in the current event
                                of.write("i_{0} = 0;\n".format(cut.name))
                                of.write(
                                    "{}_pass = std::vector<bool>({},0);\n".format(
                                        cut.name, len(cut.values)
                                    )
                                )
                                cutvalue_string = (
                                    "{"
                                    + ",".join([str(value) for value in cut.values])
                                    + "}"
                                )
                                of.write(
                                    "{}_cuts = std::vector<{}>{};\n".format(
                                        cut.name, cut.type_name, cutvalue_string
                                    )
                                )

                            # vectors that store the yields for all subsets and bin cuts
                            for i_bin_cut, bin_cut in bin_index_cuts:
                                for i_subset in subset_indices:
                                    for number_type in ["n", "sumw", "sumw2"]:
                                        of.write(
                                            "bincut{}_subset{}_{} = std::vector<{}>({}, 0);\n".format(
                                                i_bin_cut,
                                                i_subset,
                                                number_type,
                                                self.yield_type,
                                                self.ncombinations,
                                            )
                                        )

                            for i_subset in subset_indices:
                                for i_baseline, baseline_selection in enumerate(
                                    self.baseline_selections
                                ):
                                    for number_type in ["n", "sumw", "sumw2"]:
                                        of.write(
                                            "baseline{}_subset{}_{} = 0;\n".format(
                                                i_baseline,
                                                i_subset,
                                                number_type,
                                                self.yield_type,
                                                len(self.baseline_selections),
                                            )
                                        )

                            of.write(
                                "pass_bin_cuts = std::vector<bool>({},0);\n".format(
                                    len(self.bin_cuts)
                                )
                            )

            # fmt: off
            # second, generate the implementation file (actual loop)
            with open(class_name_dot_C, "w") as of:
                out = []
                p = out.append
                p("#include <TH2.h>")
                p("#include <TStyle.h>")
                p("#include <TCanvas.h>")
                p("#include <TLorentzVector.h>")
                p("")
                p("#include <iostream>")
                p("")
                p("void {classname}::Loop()")
                p("{{")
                p("  if (fChain == 0) return;")
                p("  Long64_t nentries = fChain->GetEntries(); // if i use GetEntriesFast() here, weird things happen")
                p("  bool pass_any = 0;")
                p("  int i_subset = 0;")
                p("  double rndm = 0;")
                p("  int combination_index = 0;")
                p('  fChain->SetBranchStatus("*", 0);')
                p("")
                of.write("\n".join([s.format(classname=self.get_class_name(process_name)) for s in out]))

                for branchname in [_b.GetName() for _b in chain.GetListOfBranches()]:
                    if self.branch_used(branchname):
                        _logger.info("Activating branch {}".format(branchname))
                        of.write('  fChain->SetBranchStatus("{}", 1);\n'.format(branchname))

                p = lambda s: of.write(s + "\n")
                p("")
                p("")
                p("gRandom->SetSeed({});".format(self.random_seed))
                p("  Long64_t nbytes = 0, nb = 0;")
                p("  for (Long64_t jentry=0; jentry<nentries;jentry++) {")
                p("    Long64_t ientry = LoadTree(jentry);")
                p("    if (ientry < 0) break;")
                p('    if (!(ientry%{})) std::cout << "Processing event " << ientry << "/" << nentries << std::endl;'.format(ref_process.print_status))
                p("    nb = fChain->GetEntry(jentry);")
                p("    nbytes += nb;")

                # if weight doesn't pass, continue
                of.write("    if (!({})) continue;\n".format(total_weight))

                # event passes weight - throw a dice to check into which subset it falls
                p = lambda s: of.write("    " + s + "\n")
                p("rndm = gRandom->Rndm();")
                prev_frac = 0
                if_type = "if"
                second_cmp = "<"
                for i_subset, fraction in enumerate(self.subset_fractions):
                    _logger.debug("Current subset fraction: {}".format(fraction))
                    _logger.debug("Sum of subset fractions: {}".format(sum(self.subset_fractions)))
                    fraction /= sum(self.subset_fractions)
                    fraction += prev_frac
                    if i_subset == (len(self.subset_fractions) - 1):
                        second_cmp = "<="
                    p(
                        "{if_type} ((rndm>={prev_frac})&&(rndm{second_cmp}{this_frac})) i_subset = {i_subset};".format(
                            if_type=if_type, prev_frac=prev_frac, this_frac=fraction, i_subset=i_subset, second_cmp=second_cmp,
                        )
                    )
                    if_type = "else if"
                    prev_frac = fraction
                p('else std::cout << "Event doesn\'t fall in any subset - this should not happen ..." << std::endl;')
                p("\n")

                # if restrict to subset, continue in case the event falls into a different subset
                if restrict_subset is not None:
                    p("if (i_subset != {i_subset}) continue;".format(i_subset=restrict_subset))

                # now fill the baseline selection yields
                for i_baseline, baseline_selection in enumerate(self.baseline_selections):
                    baseline_total_weight = "({})*({})".format(total_weight, baseline_selection)
                    p("if ({}) {{".format(baseline_selection))
                    for i_subset in subset_indices:
                        p("  if (i_subset=={}) {{".format(i_subset))
                        p("    baseline{}_subset{}_n += 1;".format(i_baseline, i_subset))
                        p("    baseline{}_subset{}_sumw += {};".format(i_baseline, i_subset, baseline_total_weight))
                        p("    baseline{0}_subset{1}_sumw2 += {2}*{2};".format(i_baseline, i_subset, "(" + baseline_total_weight + ")"))
                        p("  }")
                    p("}")

                # for each event, check which cut values passed
                out = []
                p = lambda s: out.append("    " + s)
                for cut in self.cuts:
                    p("pass_any = 0;")
                    p("for (i_{0}=0; i_{0}<{1}; i_{0}++) {{".format(cut.name, len(cut.values)))
                    p("  if ({0} {1} {2}_cuts[i_{2}]) {{".format(cut.varexp, cut.comparator, cut.name))
                    p("    {0}_pass[i_{0}] = 1;".format(cut.name))
                    p("    pass_any = 1;")
                    p("  }")
                    p("  else {")
                    p("    {0}_pass[i_{0}] = 0;".format(cut.name))
                    p("  }")
                    p("}")
                    # if no cut value of a single cut passed, we can skip the rest
                    p("if (!pass_any) continue;")
                of.write("\n".join(out))
                of.write("\n\n")

                # check into which bins the event might potentially fall
                p = lambda s: of.write("    " + s + "\n")
                p("pass_any = 0;")
                for i_bin_cut, bin_cut in bin_index_cuts:
                    p("if ({}) {{ ".format(bin_cut))
                    p("  pass_bin_cuts[{}] = 1;".format(i_bin_cut))
                    p("  pass_any = 1;")
                    p("} else {")
                    p("  pass_bin_cuts[{}] = 0;".format(i_bin_cut))
                    p("}")
                p("if (!pass_any) continue;")
                p("\n")

                # now for the fun part: loop over all combinations of cuts and increase the corresponding number vectors
                p = lambda s: of.write("    " + s + "\n")
                for cut_number, cut in enumerate(self.cuts):
                    p("{1:<{2}}for (i_{0}=0; i_{0}<{3}; i_{0}++) {{".format(cut.name, "", cut_number * 2, len(cut.values)))
                    p("{0:<{1}}if (!{2}_pass[i_{2}]) continue;".format("", (cut_number + 1) * 2, cut.name))

                combination_index_expr = self.get_combination_index_expr([c.name for c in self.cuts], [len(c.values) for c in self.cuts])

                p("{0:<{1}}combination_index = {2};".format("", len(self.cuts) * 2, combination_index_expr))

                # fill all vectors for all bin_cuts for all subsets
                for i_bin_cut, bin_cut in bin_index_cuts:
                    p("{0:<{1}}if (pass_bin_cuts[{2}]) {{".format("", len(self.cuts) * 2, i_bin_cut))
                    for i_subset in subset_indices:
                        p("{0:<{1}}if (i_subset=={2}) {{".format("", (len(self.cuts) + 1) * 2, i_subset))
                        p("{0:<{1}}bincut{2}_subset{3}_n[combination_index] += 1;".format("", (len(self.cuts) + 2) * 2, i_bin_cut, i_subset))
                        p("{0:<{1}}bincut{2}_subset{3}_sumw[combination_index] += {4};".format("", (len(self.cuts) + 2) * 2, i_bin_cut, i_subset, total_weight,))
                        p("{0:<{1}}bincut{2}_subset{3}_sumw2[combination_index] += {4}*{4};".format("", (len(self.cuts) + 2) * 2, i_bin_cut, i_subset, "(" + total_weight + ")",))
                        p("{0:<{1}}}}".format("", (len(self.cuts) + 1) * 2, bin_cut))
                    p("{0:<{1}}}}".format("", len(self.cuts) * 2))

                # close brackets
                for i, cut in reversed([j for j in enumerate(self.cuts)]):
                    p("{0:<{1}}}}".format("", 2 * i))

                of.write("\n")
                of.write("  }\n")
                of.write("}\n")
            # fmt: on

    def generate_and_loop(self, process_name, **kwargs):
        """
        generate the code for the given process_name and return the c scanner
        object after it was run. The kwargs are passed to generate().
        """

        self.generate(process_name, **kwargs)

        with cd(self.workdir):
            _logger.debug("Current directory: {}".format(os.getcwd()))
            _logger.debug(
                "Loading {}".format(self.get_class_name(process_name) + ".C/.h")
            )

            # can't use ".L" - since it will lead to weird results if
            # the files are deleted during a session and another class
            # is loaded - so lets use read() and gInterpreter.Declare
            code = ""
            with open(self.get_class_name(process_name) + ".h") as f:
                code += f.read()
            with open(self.get_class_name(process_name) + ".C") as f:
                code += f.read()

            if self.do_compile:
                # have to work in temp directory that is cleaned no
                # earlier than at exit, otherwise stuff will crash
                # when a new macro is loaded and an old one has been
                # deleted ...
                from .test.helpers import workInTempDir

                with workInTempDir("/tmp", cleanAtExit=True):
                    filename = "{}.C".format(self.get_class_name(process_name))
                    with open(filename, "w") as of:
                        of.write(code)
                    prev_flags = ROOT.gSystem.GetFlagsOpt()

                    # O2 seems to be default and in my checks O3
                    # didn't really bring any improvement
                    # ROOT.gSystem.SetFlagsOpt("-O3 -DNDEBUG")
                    ROOT.gSystem.SetFlagsOpt("-O2 -DNDEBUG")

                    ROOT.gROOT.ProcessLine(".L {}+".format(filename))
                    ROOT.gSystem.SetFlagsOpt(prev_flags)
                    scanner_c = getattr(ROOT, self.get_class_name(process_name))()
                    scanner_c.Loop()
            else:
                ROOT.gInterpreter.Declare(code)
                scanner_c = getattr(ROOT, self.get_class_name(process_name))()
                scanner_c.Loop()

        return scanner_c

    def scan_uproot(self, process_name, i_subset=None, i_bin=None, method_kw=None):

        "Run ahoi.scan"

        # TODO: put the following block into common function
        self.check_options()
        restrict_subset = i_subset
        restrict_bin = i_bin
        if restrict_bin is not None:
            self.check_arguments(i_bin=restrict_bin)
        if restrict_subset is not None:
            self.check_arguments(i_subset=restrict_subset)
        bin_index_cuts = []
        for i_bin_cut, bin_cut in enumerate(self.bin_cuts):
            if (restrict_bin is not None) and (not i_bin_cut == restrict_bin):
                continue
            bin_index_cuts.append((i_bin_cut, bin_cut))
        subset_indices = []
        for i_subset in range(self.n_subsets):
            if (restrict_subset is not None) and (not i_subset == restrict_subset):
                continue
            subset_indices.append(i_subset)

        # reference process for shared attributes
        ref_process = self.process_dict[process_name][0]

        total_weight = formulate.from_auto(
            "({})*({})".format(self.weight, ref_process.cut)
        )
        total_weight = formulate.to_numexpr(total_weight)

        counts, sumw, sumw2 = None, None, None
        array_dict = {}

        # loop over chunks
        _logger.info("Read in dataframe for {}".format(process_name))
        for process in self.process_dict[process_name]:
            tree = uproot.open(process.filename)[process.treename]
            branches = [b for b in tree.keys() if self.branch_used(b.decode())]
            total_chunks = len(tree) // process.print_status
            if (len(tree) % process.print_status) != 0:
                total_chunks += 1
            for chunk in tqdm(
                tree.iterate(
                    outputtype=pd.DataFrame,
                    branches=branches,
                    entrysteps=process.print_status,
                ),
                desc="{} chunks".format(process_name),
                total=total_chunks,
            ):
                w_chunk = chunk.eval(total_weight)
                if not isinstance(w_chunk, pd.Series):
                    w_chunk = np.repeat(w_chunk, len(chunk))
                else:
                    w_chunk = w_chunk.values
                mask = w_chunk != 0
                w = w_chunk[mask]
                df = chunk[mask]

                # subsets
                rnd = np.random.RandomState(seed=self.random_seed).rand(len(df))
                prev_frac = 0
                subset_masks = []
                for i_subset, fraction in enumerate(self.subset_fractions):
                    fraction /= sum(self.subset_fractions)
                    fraction += prev_frac
                    if not i_subset in subset_indices:
                        # restrict subset
                        prev_frac = fraction
                        continue
                    if i_subset == (len(self.subset_fractions) - 1):
                        subset_masks.append((rnd >= prev_frac) & (rnd <= fraction))
                    else:
                        subset_masks.append((rnd >= prev_frac) & (rnd < fraction))
                    prev_frac = fraction

                # get baseline selections
                for i_subset_array, i_subset_instance in enumerate(subset_indices):
                    for i_baseline, baseline_selection in enumerate(
                        self.baseline_selections
                    ):
                        mask = df.eval(
                            formulate.to_numexpr(
                                formulate.from_auto(baseline_selection)
                            )
                        )
                        w_baseline = w * mask
                        w_baseline *= subset_masks[i_subset_array]
                        key_n = "baseline{}_subset{}_n".format(
                            i_baseline, i_subset_instance
                        )
                        key_sumw = "baseline{}_subset{}_sumw".format(
                            i_baseline, i_subset_instance
                        )
                        key_sumw2 = "baseline{}_subset{}_sumw2".format(
                            i_baseline, i_subset_instance
                        )
                        for key in [key_n, key_sumw, key_sumw2]:
                            if not key in array_dict:
                                array_dict[key] = 0
                        array_dict[key_n] += np.count_nonzero(w_baseline)
                        array_dict[key_sumw] += w_baseline.sum()
                        array_dict[key_sumw2] += (w_baseline ** 2).sum()

                # bin cuts
                bin_cut_masks = []
                for i_bin_cut, bin_cut in bin_index_cuts:
                    mask = df.eval(formulate.from_auto(bin_cut).to_numexpr()) != 0
                    if not isinstance(mask, pd.Series):
                        mask = np.repeat(mask, len(df)).astype(np.bool)
                    else:
                        mask = mask.values
                    bin_cut_masks.append(mask)

                # run scan
                _logger.info("Scan cut combinations")
                masks_list = [
                    np.array(
                        [
                            df.eval(
                                "{} {} {}".format(
                                    formulate.to_numexpr(
                                        formulate.from_auto(cut.varexp)
                                    ),
                                    cut.comparator,
                                    val,
                                )
                            )
                            for val in cut.values
                        ],
                        dtype=np.bool,
                    )
                    for cut in self.cuts
                ]
                masks_list.insert(0, subset_masks)
                masks_list.insert(1, bin_cut_masks)

                scan_kw = dict(progress=True)
                if method_kw is not None:
                    scan_kw.update(method_kw)

                counts, sumw, sumw2 = ahoi.scan(
                    masks_list,
                    weights=w,
                    counts=counts,
                    sumw=sumw,
                    sumw2=sumw2,
                    **scan_kw
                )

        # store final arrays
        for i_bin_cut, _ in bin_index_cuts:
            for i_subset_array, i_subset_instance in enumerate(subset_indices):
                for name, val in zip(["n", "sumw", "sumw2"], [counts, sumw, sumw2]):
                    array_dict[
                        "bincut{}_subset{}_{}".format(
                            i_bin_cut, i_subset_instance, name
                        )
                    ] = val[i_subset_array][i_bin_cut].ravel()

        return array_dict

    def scan_codegen(self, process_name, **kwargs):
        method_kw = kwargs.pop("method_kw", None)  # not supported here
        scanner_c = self.generate_and_loop(process_name, **kwargs)
        array_dict = {}
        # slightly hacky extraction of the arrays and numbers
        for attr_name in dir(scanner_c):
            if attr_name.startswith("bincut"):
                array_dict[attr_name] = np.array(getattr(scanner_c, attr_name))
                getattr(scanner_c, attr_name).clear()
            if attr_name.startswith("baseline"):
                array_dict[attr_name] = getattr(scanner_c, attr_name)
        return array_dict

    def scan(self, process_name, **kwargs):
        method_dict = {
            "codegen": self.scan_codegen,
            "ahoi_uproot": self.scan_uproot,
        }
        return method_dict[self.method](
            process_name, **dict(kwargs, method_kw=self.method_kw)
        )

    def generate_and_dump(self, process_name, **kwargs):

        "Generate, run and dump to hdf5. The kwargs are passed to scan()."

        array_dict = self.scan(process_name, **kwargs)

        restrict_subset = kwargs.get("i_subset", None)
        restrict_bin = kwargs.get("i_bin", None)

        # counts, bins
        for i_bin in range(len(self.bin_cuts)):
            if (restrict_bin is not None) and (not i_bin == restrict_bin):
                continue
            for i_subset in range(self.n_subsets):
                if (restrict_subset is not None) and (not i_subset == restrict_subset):
                    continue
                for number_type in ["n", "sumw", "sumw2"]:
                    arname = "bincut{}_subset{}_{}".format(i_bin, i_subset, number_type)
                    data = array_dict[arname]
                    with h5py.File(
                        os.path.join(self.workdir, process_name + "_" + arname + ".h5"),
                        "w",
                    ) as f:
                        f.create_dataset("data", data=data)
                    del array_dict[arname]

        # baseline cuts
        for i_subset in range(self.n_subsets):
            if (restrict_subset is not None) and (not i_subset == restrict_subset):
                continue
            with h5py.File(
                os.path.join(
                    self.workdir,
                    process_name + "_baseline_subset{}".format(i_subset) + ".h5",
                ),
                "w",
            ) as f:
                for number_type in ["n", "sumw", "sumw2"]:
                    data = []
                    for i_baseline in range(len(self.baseline_selections)):
                        data.append(
                            array_dict[
                                "baseline{}_subset{}_{}".format(
                                    i_baseline, i_subset, number_type
                                )
                            ]
                        )
                    data = np.array(data)
                    f.create_dataset(number_type, data=data)

    def check_arguments(self, i_subset=0, i_bin=0, i_baseline=0, number_type="sumw"):

        if i_bin >= len(self.bin_cuts):
            raise IndexError(
                "Requested bin index {}, but only {} bin(s) available".format(
                    i_bin, len(self.bin_cuts)
                )
            )

        if i_subset >= self.n_subsets:
            raise IndexError(
                "Requested subset index {}, but only {} subset(s) available".format(
                    i_subset, self.n_subsets
                )
            )

        if i_baseline >= len(self.baseline_selections):
            raise IndexError(
                "Requested baseline index {}, but only {} baseline selection(s) available".format(
                    i_baseline, len(self.baseline_selections)
                )
            )

        if not number_type in ["n", "sumw", "sumw2"]:
            raise KeyError('Requested number "{}" not known'.format(number_type))

    def get_yields(
        self, process_name, i_subset=0, i_bin=0, number_type="sumw", cache=False
    ):

        self.check_options()

        self.check_arguments(i_subset=i_subset, i_bin=i_bin, number_type=number_type)

        options_tuple = (process_name, i_subset, i_bin, number_type)

        if options_tuple in self.yields_cache:
            _logger.debug("Found cached array for {}".format(options_tuple))
            return self.yields_cache[options_tuple]

        arname = "bincut{}_subset{}_{}".format(i_bin, i_subset, number_type)
        h5path = os.path.join(self.workdir, process_name + "_" + arname + ".h5")

        if not os.path.exists(h5path):
            _logger.info("{} doesn't exist - regenerating".format(h5path))
            self.generate_and_dump(process_name)

        with h5py.File(h5path, "r") as f:
            _logger.info("loading {}".format(h5path))
            data = f["data"][:]
            if cache:
                _logger.info("Saving array for {} to cache".format(options_tuple))
                self.yields_cache[options_tuple] = data
            return data

    def get_baseline_yields(self, process_name, i_subset, i_baseline, number_type):

        self.check_arguments(
            i_subset=i_subset, i_baseline=i_baseline, number_type=number_type
        )

        h5path = os.path.join(
            self.workdir, "{}_baseline_subset{}.h5".format(process_name, i_subset)
        )

        if not os.path.exists(h5path):
            self.generate_and_dump(process_name)

        with h5py.File(h5path, "r") as f:
            return f[number_type][i_baseline]

    def get_yields_dict(self, i_subset=0, i_bin=0):
        "Returns a dictionary of all yields arrays (n, sumw, sumw2) for all processses"
        yields_dict = {}
        for process_name in self.process_dict:
            yields_dict[process_name] = {}
            yields_dict[process_name]["n"] = self.get_yields(
                process_name, i_subset, i_bin, "n"
            )
            yields_dict[process_name]["sumw"] = self.get_yields(
                process_name, i_subset, i_bin, "sumw"
            )
            yields_dict[process_name]["sumw2"] = self.get_yields(
                process_name, i_subset, i_bin, "sumw2"
            )
        return yields_dict

    def get_sum_yields(self, *process_names, **kwargs):
        "Returns the sum for a certain number for the given processes in the given bin and subset"

        # if we want to cache a sum, don't cache the yields we need for that
        cache = kwargs.pop("cache", False)

        options_tuple = (
            "sum",
            process_names,
            tuple([(k, kwargs[k]) for k in sorted(kwargs)]),
        )

        if options_tuple in self.yields_cache:
            _logger.debug("Found cached array for {}".format(options_tuple))
            return self.yields_cache[options_tuple]

        sum_yields = sum(
            [
                self.get_yields(**dict(process_name=process_name, **kwargs))
                for process_name in process_names
            ]
        )

        if cache:
            _logger.info("Saving array for {} to cache".format(options_tuple))
            self.yields_cache[options_tuple] = sum_yields

        return sum_yields

    def get_sum_baseline_yields(self, *process_names, **kwargs):

        i_subset = kwargs.pop("i_subset", 0)
        i_baseline = kwargs.pop("i_baseline", 0)
        number_type = kwargs.pop("number_type", "sumw")
        if kwargs:
            raise KeyError("Got unexpected kwargs: {}".format(kwargs))

        return sum(
            [
                self.get_baseline_yields(
                    process_name,
                    i_subset=i_subset,
                    i_baseline=i_baseline,
                    number_type=number_type,
                )
                for process_name in process_names
            ]
        )

    @property
    def sig_process_names(self):
        return [
            process_name
            for process_name in self.process_dict
            if self.process_dict[process_name][0].is_signal
        ]

    @property
    def bkg_process_names(self):
        return [
            process_name
            for process_name in self.process_dict
            if not self.process_dict[process_name][0].is_signal
        ]

    def get_efficiencies(self, indices, *process_names, **kwargs):
        "Returns signal and background efficiencies for the given cut indices w.r.t. the given baseline selection number"

        baseline = self.get_sum_baseline_yields(*process_names, **kwargs)
        total_sumw = self.get_sum_yields(*process_names, number_type="sumw", **kwargs)[
            indices
        ]

        total_sumw[indices == -1] = np.nan

        return total_sumw / baseline

    def get_min_bkg_combinations(self, eff_sig_low, eff_sig_high, **kwargs):
        """
        Returns the cut combinations signal efficiency, background
        efficiency and combination index for which the background
        rejection is best in the given signal efficiency range.

        The signal efficiencies, background rejections and indices are
        arrays that contain all matches, in case multiple cuts yield
        the same background.
        """

        condition = kwargs.pop("condition", True)
        cache = kwargs.pop("cache", False)
        i_bin = kwargs.pop("i_bin", 0)

        total_sig = self.get_sum_baseline_yields(*self.sig_process_names, **kwargs)
        total_bkg = self.get_sum_baseline_yields(*self.bkg_process_names, **kwargs)

        i_baseline = kwargs.pop("i_baseline", 0)
        kwargs = dict(kwargs, cache=cache, i_bin=i_bin)

        # need to copy, in case this is cached
        eff_sig = np.array(self.get_sum_yields(*self.sig_process_names, **kwargs))
        eff_bkg = np.array(self.get_sum_yields(*self.bkg_process_names, **kwargs))

        eff_sig /= total_sig
        eff_bkg /= total_bkg

        combination_indices = np.where(
            (eff_sig >= eff_sig_low) & (eff_sig < eff_sig_high) & (condition)
        )[0]

        eff_bkg_in_bin = eff_bkg[combination_indices]

        if len(combination_indices) > 0:
            argmins = np.where(eff_bkg_in_bin == eff_bkg_in_bin.min())[0]
            return (
                eff_sig[combination_indices][argmins],
                eff_bkg_in_bin[argmins],
                combination_indices[argmins],
            )
        else:
            return None

    def get_ROC_curve(
        self,
        condition=None,
        binning=(0.01, 1.01, 0.01),
        i_baseline=0,
        i_subset=0,
        i_bin=0,
    ):

        sig_process_names = self.sig_process_names
        bkg_process_names = self.bkg_process_names

        if len(sig_process_names) < 1:
            raise KeyError(
                "No signal process (is_signal=True) found - can't create ROC curve"
            )
        if len(bkg_process_names) < 1:
            raise KeyError(
                "No background process (is_signal=False) found - can't create ROC curve"
            )

        total_sig = self.get_sum_baseline_yields(
            *sig_process_names, i_subset=i_subset, i_baseline=i_baseline
        )
        total_bkg = self.get_sum_baseline_yields(
            *bkg_process_names, i_subset=i_subset, i_baseline=i_baseline
        )

        _logger.info("Total bkg for baseline {}: {}".format(i_baseline, total_bkg))
        _logger.info("Total sig for baseline {}: {}".format(i_baseline, total_sig))

        # we should copy, in case this is cached
        eff_sig = np.array(
            self.get_sum_yields(*sig_process_names, i_subset=i_subset, i_bin=i_bin)
        )
        eff_bkg = np.array(
            self.get_sum_yields(*bkg_process_names, i_subset=i_subset, i_bin=i_bin)
        )

        eff_sig /= total_sig
        eff_bkg /= total_bkg

        eff_sig_bins = np.arange(*binning)
        actual_eff_sig_bins = np.empty(len(eff_sig_bins) - 1)
        eff_bkg_bins = np.empty(len(eff_sig_bins) - 1)
        combination_indices_ROC = np.empty(
            len(eff_sig_bins) - 1, dtype="int"
        )  # there are n-1 bins

        if condition is None:
            condition = True

        for i, (left, right) in enumerate(zip(eff_sig_bins, eff_sig_bins[1:])):
            _logger.debug(
                "looking for cuts that fall into the signal efficiency range {} - {}".format(
                    left, right
                )
            )
            combination_indices_bin = np.where(
                (eff_sig >= left) & (eff_sig < right) & (condition)
            )[0]
            eff_bkg_in_bin = eff_bkg[combination_indices_bin]
            eff_sig_in_bin = eff_sig[combination_indices_bin]
            if len(eff_bkg_in_bin) > 0:
                argmin = np.argmin(eff_bkg_in_bin)
                eff_bkg_bins[i] = eff_bkg_in_bin[argmin]
                actual_eff_sig_bins[i] = eff_sig_in_bin[argmin]
                combination_indices_ROC[i] = combination_indices_bin[argmin]
                _logger.debug(
                    "Found effs, effb, index = {}, {}, {}".format(
                        actual_eff_sig_bins[i],
                        eff_bkg_bins[i],
                        combination_indices_ROC[i],
                    )
                )
            else:
                eff_bkg_bins[i] = np.nan
                actual_eff_sig_bins[i] = np.nan
                combination_indices_ROC[i] = -1

        return actual_eff_sig_bins, eff_bkg_bins, combination_indices_ROC

    def get_significances(self, indices, significance_function, **kwargs):
        """
        Returns the significance values for the given cut indices.
        The given significance function has to take the total sumw of bkg,
        total sumw of signal and the statistical error sqrt(sumw2) for
        the background as argument and return the significance.
        The scalefactor can be used to normalise to luminosity.
        The event yields will also be scaled by the inverse subset fractions.
        """

        scalefactor = kwargs.pop("scalefactor", 1)

        scalefactor *= (
            sum(self.subset_fractions)
            / self.subset_fractions[kwargs.get("i_subset", 0)]
        )

        total_sig_sumw = (
            scalefactor
            * self.get_sum_yields(
                *self.sig_process_names, number_type="sumw", **kwargs
            )[indices]
        )
        total_bkg_sumw = (
            scalefactor
            * self.get_sum_yields(
                *self.bkg_process_names, number_type="sumw", **kwargs
            )[indices]
        )
        total_bkg_error = scalefactor * np.sqrt(
            self.get_sum_yields(*self.bkg_process_names, number_type="sumw2", **kwargs)[
                indices
            ]
        )

        return np.array(
            list(
                map(
                    significance_function,
                    total_sig_sumw,
                    total_bkg_sumw,
                    total_bkg_error,
                )
            )
        )


def load_from_options(workdir, **options):
    """
    Create a Scanner instance from an options dictionary in a new workdir.
    """

    # copy to avoid modification
    options = dict(options)

    kwargs = {}
    for attribute in Scanner._attributes:
        kwargs[attribute] = options.pop(attribute)

    scanner = Scanner(workdir, **kwargs)

    for process in options.pop("processes"):
        scanner.add_process(**OrderedDict(process))

    for cut in options.pop("cuts"):
        scanner.add_cut(**OrderedDict(cut))

    if options:
        raise KeyError("Found unknown options: {}".format(options))

    return scanner


def load_from_dir(workdir):
    """
    Load a Scanner instance from the options in an existing workdir
    """

    with open(os.path.join(workdir, "options.yaml")) as f:
        options = yaml.load(f, Loader=yaml.FullLoader)

    return load_from_options(workdir, **options)
