#!/usr/bin/env python
from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np

import os
import logging
import subprocess

from ahoi_root import Scanner, cd

logging.getLogger("ahoi_root").setLevel(logging.INFO)
logging.basicConfig()

if not os.path.exists("tmva_class_example.root"):
    subprocess.call(["wget", "http://root.cern.ch/files/tmva_class_example.root"])

# we will scan on two independend random subsets
# 0: train
# 1: test
scn = Scanner("onebin", n_subsets=2, method="ahoi_uproot")

# one could give a weight expression here, for example:
#scn = Scanner("onebin", n_subsets=2, weight="0.5*var4")


scn.add_process("bkg", "tmva_class_example.root", "TreeB", is_signal=False, print_status=1000)
scn.add_process("sig", "tmva_class_example.root", "TreeS", is_signal=True, cut="0.03") # the cut in this example just a scalefactor to scale down the signal

# scan both lower and upper cuts for all 4 variables
scn.add_cut("var1", [-3+i for i in range(7)])
scn.add_cut("var1", [-3+i for i in range(7)], name="var1_upper", comparator="<")
scn.add_cut("var2", [-3+i for i in range(7)])
scn.add_cut("var2", [-3+i for i in range(7)], name="var2_upper", comparator="<")
scn.add_cut("var3", [-3+i for i in range(7)])
scn.add_cut("var3", [-3+i for i in range(7)], name="var3_upper", comparator="<")
scn.add_cut("var4", [-3+i for i in range(7)])
scn.add_cut("var4", [-3+i for i in range(7)], name="var4_upper", comparator="<")

print("Number of cut combinations: ", scn.ncombinations)

effs, effb, roc_indices = scn.get_ROC_curve()

# one could give a constraint on the cut combinations
# - for example a max statistical uncertainty of 50%
# total_bkg_sumw = scn.get_sum_yields(*scn.bkg_process_names, number_type="sumw")
# total_bkg_error = np.sqrt(scn.get_sum_yields(*scn.bkg_process_names, number_type="sumw2"))
# effs, effb, roc_indices = scn.get_ROC_curve(condition=(total_bkg_error<0.5*total_bkg_sumw))

# one could also change the range of the efficiencies scanned for the
# ROC curve, for example (arguments are for np.arange - start, stop, step):
# effs, effb, roc_indices = scn.get_ROC_curve(binning=(0.001, 0.2, 0.001))

# evaluate efficiencies also on the independent second subset
effs_test = scn.get_efficiencies(roc_indices, *scn.sig_process_names, i_subset=1)
effb_test = scn.get_efficiencies(roc_indices, *scn.bkg_process_names, i_subset=1)

fig, ax = plt.subplots()
ax.plot(effs, 1-effb, label="train")
ax.plot(effs_test, 1-effb_test, label="test")

# one could also sort the test efficiencies
# (since signal efficiencies might have changed as well!)
# (if the values should be connected with lines in the plot)
# argsort = np.argsort(effs_test)
# ax.plot(effs_test[argsort], 1-effb_test[argsort], label="test")

ax.legend()
ax.set_xlabel("signal efficiency")
ax.set_ylabel("background rejection")

fig.savefig("roc_curve.pdf")

#
# now calculate the significance for all points on the ROC curve
# the functions take s (signal yield), 
#

import ROOT
def binomial_significance(s, b, db):
    return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(s, b, db/b)

def binomial_flat30p_significance(s, b, db):
    return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(s, b, 0.3)


import math
def asimov_significance(s, b, db):
    """
    significance based on asymptotic profile likelihood with poisson constraint
    (see `<http://www.pp.rhul.ac.uk/~cowan/stat/medsig/medsigNote.pdf>`_)
    """
    return math.sqrt(2*((s+b)*math.log(((s+b)*(b+db**2))/(b**2+(s+b)*db**2))-(b**2)/(db**2)*math.log(1+(db**2*s)/(b*(b+db**2)))))


significances_binomial_train = scn.get_significances(roc_indices, binomial_significance, i_subset=0)
significances_binomial_test = scn.get_significances(roc_indices, binomial_significance, i_subset=1)
significances_binomial_flat30p_train = scn.get_significances(roc_indices, binomial_flat30p_significance, i_subset=0)
significances_binomial_flat30p_test = scn.get_significances(roc_indices, binomial_flat30p_significance, i_subset=1)
significances_asimov_train = scn.get_significances(roc_indices, asimov_significance, i_subset=0)
significances_asimov_test = scn.get_significances(roc_indices, asimov_significance, i_subset=1)

# one could also give a luminosity scalefactor (subset fractions will be taken into account automatically), for example:
# significances_asimov_test = scn.get_significances(roc_indices, asimov_significance, i_subset=1, lumifactor=140000)

fig, ax = plt.subplots()
ax.plot(effs, significances_binomial_train, "b-", label="binomial (train)")
ax.plot(effs_test, significances_binomial_test, "b--", label="binomial (test)")
ax.plot(effs, significances_binomial_flat30p_train, "r-", label="binomial flat 30% (train)")
ax.plot(effs_test, significances_binomial_flat30p_test, "r--", label="binomial flat 30% (test)")
ax.plot(effs, significances_asimov_train, "g-", label="asimov (train)")
ax.plot(effs_test, significances_asimov_test, "g--", label="asimov (test)")
ax.legend(loc="lower right", framealpha=0.5)
ax.set_xlabel("signal efficiency")
ax.set_ylabel("significance")
fig.savefig("z_vs_effs.pdf")

#
# Lets store the cut expressions for the ROC curve, together with the yields and efficiencies
# Here we will sum both subsets (train and test) together (but still store the efficiencies for the train set only)
# Note that the significances can be higher due to lower statistical uncertainty when combining train/test
# This should be automated a bit more ...
#

# yields (np arrays) for each cut combination
total_sig_sumw = (
    scn.get_yields(*scn.sig_process_names, i_subset=0)
    + scn.get_yields(*scn.sig_process_names, i_subset=1)
)
total_bkg_sumw = (
    scn.get_yields(*scn.bkg_process_names, i_subset=0)
    + scn.get_yields(*scn.bkg_process_names, i_subset=1)
)
total_bkg_sumw2 = (
    scn.get_yields(*scn.bkg_process_names, number_type="sumw2", i_subset=0)
    + scn.get_yields(*scn.bkg_process_names, number_type="sumw2", i_subset=1)
)

with open("cuts.txt", "w") as of:
    for i, (cut_index, effs_cut, effb_cut) in enumerate(zip(roc_indices, effs, effb)):
        of.write("\n")
        of.write("effs_train={}:\n".format(effs_cut))

        if cut_index <= 0:
            of.write("-\n")
            continue

        s = total_sig_sumw[cut_index]
        b = total_bkg_sumw[cut_index]
        db = math.sqrt(total_bkg_sumw2[cut_index])

        of.write(scn.get_cut_expr(cut_index)+"\n")
        of.write("cut_index={}, effb_train={}, s={}, b={} +/- {}, z={}\n".format(
            cut_index, effb_cut, s, b, db, asimov_significance(s, b, db)
        ))

