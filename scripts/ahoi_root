#!/usr/bin/env python

"""
This script helps to perform a range of standard actions on a already configured workdir
"""

import argparse

from ahoi_root import load_from_dir

parser = argparse.ArgumentParser(description="Perform a range of standard actions on a configured ahoi workdir")
parser.add_argument("workdir")
parser.add_argument("-i", type=int, help="cut combination index")
parser.add_argument("-e", "--expr", help="cut expression")
parser.add_argument("-b", "--bin", help="bin index", default=0, type=int)
parser.add_argument("-s", "--subset", help="subset index", default=0, type=int)
parser.add_argument("-p", "--baseline", help="baseline (preselection) index", default=0, type=int)
parser.add_argument("-m", "--print-min-bkg",
                    help="print information about the cut combinations that minimise background within a given signal efficiency range",
                    nargs=2, metavar=("eff_sig_low", "eff_sig_high"), type=float)
parser.add_argument("--print-expr", action="store_true", help="print cut expression for the given cut combination index")
parser.add_argument("--print-ncombinations", action="store_true", help="print the total number of cut combinations")
parser.add_argument("--draw", action="store_true", help="print the yields using TTree::Draw for the given cut expr (-e) or cut combination index (-i)")
args = parser.parse_args()

scn = load_from_dir(args.workdir)

if args.print_ncombinations:
    print(scn.ncombinations)

if args.print_expr:
    if args.i is None:
        parser.error("Need to provide -i if using --cut-expr")
    print(scn.get_cut_expr(args.i))

if args.draw:
    if args.i is not None and args.expr is not None:
        parser.error("Can't provide both cut combination index and cut expression")
    if args.i is not None:
        expr = scn.get_cut_expr(args.i)
    else:
        if args.expr is None:
            parser.error("Need to provide either a cut combination index (-i) or a cut expression (-e) for --draw")
        expr = args.expr
    for process_name in scn.process_dict:
        print("{}: ".format(process_name))
        print(scn.get_yields_draw(process_name, expr, bin_index=args.bin))


if args.print_min_bkg is not None:
    effs_low, effs_high = args.print_min_bkg
    min_combinations = scn.get_min_bkg_combinations(effs_low, effs_high, i_bin=args.bin, i_subset=args.subset, i_baseline=args.baseline)
    if min_combinations is not None:
        ar_effs, ar_effb, indices = min_combinations
        print("The following cut combinations minimise the background for a signal efficiency between {} and {}".format(effs_low, effs_high))
        for effs, effb, index in zip(ar_effs, ar_effb, indices):
            print("")
            print("Cut combination {}:".format(index))
            print("Expression: {}".format(scn.get_cut_expr(index)))
            print("Sig efficiency: {}".format(effs))
            print("Bkg efficiency: {}".format(effb))
    else:
        print("No cut combinations give a signal efficiency in the requested range ({} -{})".format(effs_low, effs_high))


